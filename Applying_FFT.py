## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## SCRIPT: Applying_FFT.py -> To apply the FFT to a signal.
## DATE: 28/08/2018
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


temp = 2 # Seconds
srate = 200 # 200 samples per second
# For example, if we want 10 seconds of signal with 200 samples per second, we will proceed from 2000 samples.
nSamples = temp * srate

sX = np.linspace(0.0, temp, nSamples)
#print(sX)

plt.figure("FFT Example")

plt.subplot(2,1,1)
x = np.linspace(0.0, temp, nSamples)
y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
plt.xlabel("Time (s)")
plt.ylabel("Voltage (V)")
plt.plot(x,y)

plt.subplot(2,1,2)
yf = fft(y)
xf = np.linspace(0.0, 1.0/(2.0/srate), nSamples//2) # // -> Rounds down. Always will be an integer. Make //2 because the signal is mirrored.
plt.xlabel("Frequency (Hz)")
plt.ylabel("Gain")
plt.plot(xf, 2.0/nSamples * np.abs(yf[0:nSamples//2])) # np.abs() -> Absolute. Transform all negative numbers into positive numbers.

plt.grid()
plt.show()
