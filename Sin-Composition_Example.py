## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## SCRIPT: Sin-Composition.py -> Sin composition example
## DATE: 28/08/2018
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


import numpy as np
from matplotlib import pyplot as plt


temp2 = 5 # Seconds
srate2 = 200 # 200 samples per second
# For example, if we want 10 seconds of signal with 200 samples per second, we will proceed from 2000 samples.
nSamples2 = temp2 * srate2

sX2 = np.linspace(0.0, temp2, nSamples2)

sins = np.sin((2*np.pi*1*sX2))
for freqs in range(2,1002): # 2,1002
    sins = np.sin((2*np.pi*freqs*sX2)) + sins

# Plot
plt.figure("Sin composition example")
plt.grid(True)
plt.title("Sines with " + str(srate2) + " SPS")
plt.ylabel("Amplitude")
plt.xlabel("Time [s]")
plt.plot(sX2, sins, color="orange")
plt.show()