## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## SCRIPT: Aliasing_Example.py -> Aliasing example with sines.
## DATE: 28/08/2018
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


import numpy as np
from matplotlib import pyplot as plt


temp = 1 # Seconds
srate = 200 # 200 samples per second
# For example, if we want 10 seconds of signal with 200 samples per second, we will proceed from 2000 samples.
nSamples = temp * srate

sX = np.linspace(0.0, temp, nSamples)
#print(sX)

freq1 = 1 # Hz
sin1 = np.sin((2*np.pi*freq1*sX) + np.pi/2)

freq2 = 5 # Hz
sin2 = np.sin(2*np.pi*freq2*sX + np.pi/2)

# Aux sin
xAux = np.linspace(0.0, temp, 7)
yAux = np.sin(2*np.pi*xAux + np.pi/2)

# Plot
plt.figure("Aliasing Example")
plt.grid(True)
plt.title("Sines with " + str(srate) + " SPS")
plt.ylabel("Amplitude")
plt.xlabel("Time [s]")
plt.plot(sX, sin1, color="orange")
plt.plot(sX, sin2, color="blue")
plt.scatter(xAux,yAux, color="black", alpha=0.5)
plt.show()
