## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## SCRIPT: ButterFilter_Responses.py -> To get Butterworth filters response.
## DATE: 28/08/2018
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


from scipy.signal import butter, lfilter, lfilter_zi
from scipy.fftpack import fft
import matplotlib.pyplot as plt
import numpy as np


## FUNCTIONS -----------------------------------------------------------------------------------
# Buterworth filter
def butter_bandpass(lowcut, highcut, srate, order=5):
    nyq = 0.5 * srate
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a
#end def

def butter_bandpass_filter(data, lowcut, highcut, srate, order=5):
    b, a = butter_bandpass(lowcut, highcut, srate, order=order)
    y = lfilter(b, a, data)
    return y
#end def

# Get signals from file
def getPPGSignal():

    sps = 200 # Samples per second
    REDsignal, IRsignal, synced = [], [], []
    signalBase = 1.045

    patient = "paciente1"
    fileDir = patient + "/uppg_signals.csv"

    samples = 0
    with open(fileDir) as dataFile:
        next(dataFile)
        for line in dataFile:
            aux = line.split(',')
            REDsignal.append(signalBase - float(aux[0]))
            IRsignal.append(signalBase - float(aux[1]))
            synced.append(float(aux[2]))
            samples +=1
        #end-for
    #end-with

    dataFile.close()

    return REDsignal, IRsignal, samples, sps
#end def

def getECGPPGSignal():

    sps = 200 # Samples per second
    ECGsignal, PPGsignal, ann = [], [], []

    patient = "paciente1"
    fileDir = patient + "/uppg_signals.csv"

    samples = 0
    with open(fileDir) as dataFile:
        next(dataFile)
        for line in dataFile:
            aux = line.split(',')
            ECGsignal.append(float(aux[0]))
            PPGsignal.append(float(aux[1]))
            ann.append(float(aux[2]))
            samples +=1
        #end-for
    #end-with

    dataFile.close()

    return ECGsignal, PPGsignal, samples, sps
#end def


# MAIN -----------------------------------------------------------------------------------------

# Read signal from file ---------------------------
#RED, IR, nSamples, srate = getPPGSignal() # PPG
IR, RED, nSamples, srate = getECGPPGSignal() # ECG e PPG

# Applying FFT ------------------------------------
yfft = fft(IR)
xfft = np.linspace(0.0, 1.0/(2.0/srate), nSamples//2)

plt.figure("PPG signal FFT")

plt.xlabel("Frequency (Hz)")
plt.ylabel("Gain")
plt.plot(xfft, 2.0/nSamples * np.abs(yfft[0:nSamples//2]), label='IR Signal FFT')



# Applying bandpass filter ------------------------
# Desired cutoff frequency (in Hz) and filter order.
lowcut = 0.5
highcut = 10
orders = 2

x = np.linspace(0, nSamples/srate, nSamples, endpoint=True)

plt.figure("PPG raw and filtered signal")

plt.subplot(2,1,1)
y = IR
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
plt.plot(x,y, label='Noisy signal')


plt.subplot(2,1,2)
yf = butter_bandpass_filter(IR, lowcut, highcut, srate, order=orders)
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
plt.plot(x, yf, label='Filtered signal')

plt.grid()
plt.show()
