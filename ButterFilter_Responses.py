## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## SCRIPT: ButterFilter_Responses.py -> To get Butterworth filters response.
## DATE: 28/08/2018
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


from scipy.signal import butter, lfilter, freqz
import scipy.fftpack
import matplotlib.pyplot as plt
import numpy as np


## FUNCTIONS -----------------------------------------------------------------------------------
class lowPass:
    def butter_lowpass(lowcut, srate, order=5):
        nyq = 0.5 * srate
        low = lowcut / nyq
        b, a = butter(order, low, btype='low')
        return b, a
    #end def

    def butter_lowpass_filter(data, lowcut, srate, order=5):
        b, a = lowPass.butter_lowpass(lowcut, srate, order=order)
        y = lfilter(b, a, data)
        return y
    #end def
#end class

class highPass:
    def butter_highpass(highcut, srate, order=5):
        nyq = 0.5 * srate
        high = highcut / nyq
        b, a = butter(order, high, btype='high')
        return b, a
    #end def

    def butter_highpass_filter(data, highcut, srate, order=5):
        b, a = highPass.butter_highpass(highcut, srate, order=order)
        y = lfilter(b, a, data)
        return y
    #end def
#end class

class bandPass:
    def butter_bandpass(lowcut, highcut, srate, order=5):
        nyq = 0.5 * srate
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a
    #end def

    def butter_bandpass_filter(data, lowcut, highcut, srate, order=5):
        b, a = bandPass.butter_bandpass(lowcut, highcut, srate, order=order)
        y = lfilter(b, a, data)
        return y
    #end def
#end class


## MAIN ----------------------------------------------------------------------------------------
srate = [200] # Signal sample rate [Hz]
lowcut = [0.1, 0.20, 0.5] # Low cut frequencys [Hz]
highcut = [8, 10, 14] # High cut frequencys [Hz]
orders = [1, 2, 4] # Filter orders
'''

# HighPass Filter
for sr in srate: 
    for hcf in highcut:
                
            fileName = "response-" + str(sr) + "SPS_" + str(hcf) + "Hz-high"

            plt.figure(fileName, figsize=(14,6))
            for order in orders:
                b, a = highPass.butter_highpass(hcf, sr, order=order)
                w, h = freqz(b, a, worN=50000)
                plt.plot((sr * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)
            #end-for

            plt.plot([0, 0.5 * sr], [np.sqrt(0.5), np.sqrt(0.5)], '--', label='sqrt(0.5)')
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Gain')
            plt.grid(True)
            plt.legend(loc='best')

            print(fileName)
            plt.show()
        #end-for
    #end-for
#end-for

'''

# LowPass Filter
for sr in srate: 
    for lcf in lowcut:
                
            fileName = "response-" + str(sr) + "SPS_" + str(lcf) + "Hz-low"

            plt.figure(fileName, figsize=(14,6))
            for order in orders: 
                b, a = lowPass.butter_lowpass(lcf, sr, order=order)
                w, h = freqz(b, a, worN=50000)
                plt.plot((sr * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)
            #end-for

            plt.plot([0, 0.5 * sr], [np.sqrt(0.5), np.sqrt(0.5)], '--', label='sqrt(0.5)')
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Gain')
            plt.grid(True)
            plt.legend(loc='best')

            print(fileName)
            plt.show()
        #end-for
    #end-for
#end-for

'''

# BandPass Filter
for sr in srate: 
    for lcf in lowcut:
        for hcf in highcut:
        
            fileName = "response-" + str(sr) + "SPS_" + str(lcf) + "Hz-low_" + str(hcf) + "Hz-high"

            plt.figure(fileName, figsize=(14,6))
            for order in orders: 
                b, a = bandPass.butter_bandpass(lcf, hcf, sr, order=order)
                w, h = freqz(b, a, worN=50000)
                plt.plot((sr * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)
            #end-for

            plt.plot([0, 0.5 * sr], [np.sqrt(0.5), np.sqrt(0.5)], '--', label='sqrt(0.5)')
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Gain')
            plt.grid(True)
            plt.legend(loc='best')

            print(fileName)
            plt.show()
        #end-for
    #end-for
#end-for

'''